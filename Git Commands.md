Git Commands
=======

## WORKFLOW

**Prerequisite: Setup the repo.**

0. Create a new branch off the master branch, named after the feature you're working on.
0. Checkout that new branch.
0. Code, code, code.
0. Commit your changes when the feature is complete.
    * Or, if your feature isn't complete but you have to work on something else or work on another branch: stash your changes, and restore them later to *any* branch.
0. Checkout master.
0. Rebase or pull the latest from the remote master so you're up-to-date.
0. Merge your feature branch into the local master branch.
0. Resolve conflicts, if any, and commit that resolution as a new commit.
0. Push up your changes:
    * For pushing straight to master:
        0. Push the master branch to the remote origin master.
    * For a pull request:
        0. Push your changes to your branch's remote branch.
        0. Make the pull request from your remote branch into the remote master branch.
0. Delete your local branch.
    * Delete your remote feature branch if it's been merged into the remote master.

*Rinse, and repeat.*

---

### SETUP (GLOBAL SETTINGS)
* `git config --global user.name "Your Name"` (Set username globally on your system)
* `git config --global user.email "your@e.mail"` (Set email globally on your system)

### SETUP (EXISTING REPO)
* `git clone https://the/url` (Clone an existing repo)

### SETUP (NEW REPO)
* `git init` (Initialize a repo if *not* cloning)
* `git remote add origin https://the/url` (Add remote origin if *not* cloning)

### COMMITTING (Committing flow for when a feature is complete)
* `git diff` (Shows current changes)
* `git add -A` (Stages *all* changes for a commit)
    * Or, `git add path\to\file.js path\to\another-file.js` for one or more files
* `git status` (Shows the staged changes)
* `git commit -m "Some message"` (Sets a checkpoint for all files and stamps the changes with a unique ID)
    * Or, `git commit path\to\file.js path\to\another-file.js -m "Some message"` to only commit certain files
* `git pull some-branch` (Merges some branch into yours and creates conflicts that need to be addressed, if any)
* `git pull origin some-branch` (Merges a remote branch into your local branch)
* `git push origin some-branch` (Pushes the changes to a remote branch)
* `git log` (Shows commit log; then `enter` to expand more, and `q`, `z`, or `ctrl + z` to quit the log)

### STASHING (When the feature is incomplete)
*FYI: Stashed changes are branch-agnostic.*

* `git stash` (Stores all non-committed changes away temporarily, but _not_ untracked and ignored files)
* `git stash --all` (Stores all non-committed changes, _including_ untracked and ignored files)
* `git stash save "Some message"` (Stores all non-committed changes away temporarily with a message)
* `git stash show` (Shows the changes as a diff)
* `git stash show stash@{0}` (Shows the changes as a diff for the index-specified stash)
* `git stash pop` (Restores the most recent stash and deletes it from the stash array)
* `git stash apply stash@{0}` (Restores the index-specified stash; but, does *not* remove it from the stash array)
* `git stash list` (Lists all stashed changesets)
* `git stash drop` (Drops the most recent stash)
* `git stash drop stash@{0}` (Drops the index-specified stash)
* `git stash clear` (Drops all stashes)

### BRANCHING
* `git branch` (Lists the local branches)
* `git branch a-new-branch` (Creates a local branch, but doesn't switch to it!)
* `git checkout some-branch` (Switch to a branch)
* `git checkout -b a-new-branch` (Create & checkout a local branch)
* `git branch -d some-branch` (Delete a local branch)
* `git branch -m current-branch-new-name` (Rename the current local branch)
    * NOTE: The remote branch with the previous name is now unused
* `git branch -M current-branch-new-name` (Rename the current local branch if only the capitalization has changed)
    * NOTE: The remote branch with the previous name is now unused
* `git branch -m some-branch new-name` (Rename any local branch)
    * NOTE: The remote branch with the previous name is now unused
* `git push origin some-branch` (Push to a remote branch)
* `git push origin --delete some-branch` (Delete a remote branch)

### LOGGING
* `git log` (Show log history and which branches the current branch came from)
* `git log --all --decorate --oneline --graph` (Shows all logs and branches graphically)

### PULLING
* `git rebase origin/master` (Merges the remote master branch and puts commits in the working branch as the latest/newest commits of the master branch)
* `git rebase -i origin/master` (Same as above, but allows editing the commit messages)
* `git pull some-branch` (Merges some branch into yours and creates conflicts that need to be addressed, if any)
* `git pull origin some-branch` (Merges a remote branch into your local branch)
* `git pull --allow-unrelated-histories another-repo-reference master` (Merge from another remote repo)

### MERGING
* `git merge branch-to-pull-merge-from` (Merge a branch into the current branch)
* `git merge origin branch-to-pull-merge-from` (Merge a remote branch into the current local branch)
* To consolidate all commits of a branch into one commit on master:
    0. `git checkout master`
    0. `git merge --squash some-branch`
    0. `git commit` (Omitting the `-m` flag will allow editing of all the commits messages combined)

### PUSHING
* `git push origin some-branch` (Pushes the changes to a remote branch)

### FIXES
**NOTE: It's a safe practice to backup/zip your repos after each commit, or feature merge, just in case some git commands you try critically mess up your repo or its history.**

NOTE: Make sure there are no programs open that are referencing your repo. It can cause unexpected behavior and prevent Git from working properly when you try again after the programs are closed.

* Change the commit message of the previous commit
    0. `git commit --amend -m "Some message"`
    0. `git push origin master -f`
* Make a minor change and include it in the last commit
    0. `git commit --amend` or `git commit --amend --no-edit` (to keep the commit message the same)
    0. `git push origin master -f`
* `git reset --soft HEAD~` (Undo the last commit)
* `git remote set-url origin https://the/url` (Change the remote origin, if necessary)
* `git clean -fd` (Remove untracked files and directories)
* `git checkout .` (Ignore changes since last commit in the current working directory)
* `git reset HEAD --hard` then `git clean -fd` (Ignore all changes since last commit in the current working directory and remove untracked files and directories)
* `git fetch origin some-branch` then `git reset --hard FETCH_HEAD` (Get the latest and replace everything with remote branch)
* `git reset --hard origin/some-branch` (Replace everything with remote branch; but, only up to the latest commit the local branch is on)
* `git push origin some-branch -f` (Replaces the remote branch ignoring conflicts)
* `git checkout branch-to-replace-from path/to/the/file.extension` (Replaces the file, ignoring conflicts)
    * May need to `git add -A`, `git commit -m ""`, and then `git reset` to remove the commit itself (?) and return to the HEAD
* Replace entire git history, but keep the git config, and replace the remote branch:
    0. `git checkout --orphan new-branch` (Create a new branch that acts like an empty git init)
    0. `git add -A` (Add all the files as if doing an initial commit)
    0. `git commit -m "Initial commit."` (Commit all the files as an initial commit)
    0. `git branch -D master` (Delete the old "master" branch)
    0. `git branch -m master` (Rename the "new-branch" to "master")
    0. `git gc --aggressive --prune=all` (Remove old history/files)
    0. `git push -f origin master`  (Force push to master)
* Revert to a previous commit but keep all commits:
    0. `git revert theCommitId`
* Revert to a previous commit, commit it, and don't impact history:
    0. `git revert --no-commit 0766c053..HEAD`
    0. `git commit`
    0. From [Stackoverflow](https://stackoverflow.com/questions/4114095/how-to-revert-a-git-repository-to-a-previous-commit#answer-21718540)
        * This will revert everything from the HEAD back to the commit hash, meaning it will recreate that commit state in the working tree as *if* every commit since had been walked back. You can then commit the current tree, and it will create a brand new commit essentially equivalent to the commit you "reverted" to.
        * (The `--no-commit` flag lets git revert all the commits at once- otherwise you'll be prompted for a message for each commit in the range, littering your history with unnecessary new commits.)
        * This is a **safe and easy way to rollback to a previous state**. No history is destroyed, so it can be used for commits that have already been made public.
* Revert to a previous commit and blow away all later commits:
    0. `git reset theCommitId` (Go back to the specified commit)
    0. `git reset --soft HEAD@{1}` (Point back to the commit HEAD)
    0. `git commit -m "Revert to theCommitId"` (Make a commit, though the same as the reverted commit)
    0. `git reset --hard` (Hard reset to that commit)
    0. `git clean -fd` (Not sure if cleaning untracked files and directories is needed, but hey)
    0. `git push -f origin master` (Force push the changes to master)
* Merge another branch into master treating it as if it was essentially the "new master". This may be most helpful if the other branch was ahead of master because of a master reversion. (FYI: If the feature branch was ahead of master just because of normal commits, then simply merging master into the feature branch, and then merging that feature branch back into master would be sufficient).
    0. `git checkout some-branch` (Checkout the feature branch like normal)
    0. `git merge -s ours master` (Merge in master but preferring some-branch's changes over master's changes)
    0. `git checkout master` (Checkout master branch like normal)
    0. `git merge some-branch` (Merge some-branch like normal)
* If git says "Everything up-to-date" during a `git push origin master`, then you're probably on the wrong branch, forgot to commit your changes first, or checked out a previous commit.
